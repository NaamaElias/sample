// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB2ASyG-Zqfaa-hjFUTXSzcrmpJnPK29uY",
    authDomain: "sample-naama.firebaseapp.com",
    projectId: "sample-naama",
    storageBucket: "sample-naama.appspot.com",
    messagingSenderId: "380114063097",
    appId: "1:380114063097:web:098f4e4dbd22cb935c1c28"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
