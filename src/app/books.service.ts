import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import {AngularFirestore, AngularFirestoreCollection}from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})
export class BooksService {
  [x: string]: any;

  // books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"ummy text of the printing and typesetting industry. Lorem Ipsum has been"},
  // {title:'War and Peace', author:'Leo Tolstoy', summary:"ummy text of the printing and typesetting industry. Lorem Ipsum has been"}, 
  // {title:'The Magic Mountain', author:'Thomas Mann', summary:"ummy text of the printing and typesetting industry. Lorem Ipsum has been"}];

  // // public getBooks(){
  // //   return this.books;
  // // }

  // public getBooks(){
  //   const booksObservable = new Observable(obs => {
  //     setInterval(()=>obs.next(this.books),500)
  //   });
  //   return booksObservable;
  // }

  bookCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  /*
  public getBooks(userId){
    this.bookCollection = this.db.collection(`users/${userId}/books`); 
    return this.bookCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data(); 
          data.id = document.payload.doc.id;
          return data; 
        }
      )
    ))
  }*/

  public getBooks(userId){
      this.bookCollection = this.db.collection(`users/${userId}/books`,
      ref => ref.orderBy('title', 'asc').limit(4)); 
      return this.bookCollection.snapshotChanges()
    }
  
  nextPage(userId,startAfter): Observable<any[]>{
    this.bookCollection = this.db.collection(`users/${userId}/books`, 
    ref => ref.limit(4).orderBy('title', 'asc')
      .startAfter(startAfter))    
    return this.bookCollection.snapshotChanges();
  }

    prevPage(userId,startAt): Observable<any[]>{
      this.bookCollection = this.db.collection(`users/${userId}/books`, 
      ref => ref.limit(4).orderBy('title', 'asc')
        .startAt(startAt))    
      return this.bookCollection.snapshotChanges();
    }

  deteleBook(Userid:string ,id:string){
    this.db.doc(`users/${Userid}/books/${id}`).delete();
  }

  updateBook(userId:string,id:string, title:string, author:string){
    this.db.doc(`users/${userId}/books/${id}`).update(
      {
        title:title,
        author:author
      }
    )
  }

  addBook(userId:string, title:string, author:string){
    const book = {title:title, author:author};
    this.userCollection.doc(userId).collection('books').add(book);
  }

  constructor(private db:AngularFirestore) { }
}
