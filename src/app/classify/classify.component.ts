import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})
export class ClassifyComponent implements OnInit {

  favoriteNew: string;
  news: string[] = ['BBC', 'CNN', 'NBC'];

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.favoriteNew = this.route.snapshot.params.new;
  }

}
